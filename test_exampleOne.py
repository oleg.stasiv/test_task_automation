import pytest


@pytest.mark.usefixtures("setup")
class TestExampleOne:

    def test_title(self):
        # Sign in
        username = self.driver.find_element_by_id("login_field")
        username.send_keys("stasiv.oleh@gmail.com")
        password = self.driver.find_element_by_id("password")
        password.send_keys("Go80688425252")
        signin_btn = self.driver.find_element_by_xpath("//input[@name='commit']")
        signin_btn.submit()

        # Find repository "pytes"
        search_field = self.driver.find_element_by_xpath("//div[@class='position-relative']/form//input[@type='text']")
        search_field.send_keys("pytest")
        search_field.submit()

        # Go to repository "pytes"
        pytest_link = self.driver.find_element_by_xpath("//ul[@class='repo-list']//li//a[@href='/pytest-dev/pytest']")
        pytest_link.click()

        # Count folders and files into root directory
        folders = self.driver.find_elements_by_xpath(
            "//tr[@class='js-navigation-item']//*[local-name() = 'svg' and @aria-label='directory']")

        files = self.driver.find_elements_by_xpath(
            "//tr[@class='js-navigation-item']//*[local-name() = 'svg' and @aria-label='file']")

        print("FOLDERS - " + str(len(folders)))
        print("FILES - " + str(len(files)))
